using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OkCancelMessageBox : MonoBehaviour
{
    public event Action onOKClicked;
    public event Action onCancelClicked;

    [SerializeField] private string _title;
    [SerializeField] private string _description;

    [SerializeField] private Text _titleObject;
    [SerializeField] private Text _descriptionObject;

    public void Initialize()
    {
        _titleObject.text = _title;
        _descriptionObject.text = _description;
    }

    public void Initialize(string description)
    {
        _titleObject.text = _title;
        _descriptionObject.text = description;
    }

    public void OkClicked()
    {
        onOKClicked?.Invoke();
        Close();
    }

    public void CancelClicked()
    {
        onCancelClicked?.Invoke();
        Close();
    }

    private void Close()
    {
        gameObject.SetActive(false);
    }
}