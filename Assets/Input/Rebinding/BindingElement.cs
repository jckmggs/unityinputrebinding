using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace PlayerInput.Rebinding
{
    //Usage:
    //Ensure that a playerInputRebinder exists
    //Drop prefab onto scene. Then, in the inspector:
    //  Assign the PlayerInputRebinder
    //  Assign _inputActionRef to the associated action which this element can rebind
    //  Assign a display name
    public class BindingElement : MonoBehaviour
    {
        private enum BindingType
        {
            KeyboardAndMouse = 0,
            Gamepad = 1
        }

        [SerializeField] private BindingType _controlScheme;
        [SerializeField] private string _actionDisplayName;

        [Header("References")]
        [SerializeField] private PlayerInputRebinder _inputRebinder;
        [SerializeField] private InputActionReference _inputActionRef;
        [SerializeField] private Text _actionText;
        [SerializeField] private Text _rebindText;
        [SerializeField] private Button _rebindButton;

        private void Start()
        {
            _actionText.text = _actionDisplayName;
            UpdateBindingText();
        }

        private void OnEnable()
        {
            _rebindButton.onClick.AddListener(() => DoRebind());
            _inputRebinder.rebindComplete += UpdateBindingText;
            _inputRebinder.rebindCancelled += UpdateBindingText;
            _inputRebinder.refresh += UpdateBindingText;
        }

        private void OnDisable()
        {
            _inputRebinder.rebindComplete -= UpdateBindingText;
            _inputRebinder.rebindCancelled -= UpdateBindingText;
            _inputRebinder.refresh -= UpdateBindingText;
        }

        private void DoRebind()
        {
            _rebindText.text = "Press a button";
            int bindingIndex = (int)_controlScheme;
            _inputRebinder.DoRebind(_inputActionRef.action.name, bindingIndex, _rebindText);
        }

        private void UpdateBindingText()
        {
            int bindingIndex = (int)_controlScheme;
            _rebindText.text = _inputRebinder.GetBindingName(_inputActionRef.action.name, bindingIndex);
        }
    }
}