using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using System;

namespace PlayerInput.Rebinding
{
    public interface IInputSource
    {
        public void Initialize(PlayerInputActions inputActions);
    }

    //Overlapping bindings isn't used yet, should use to check with user before applying changes

    //Warn the user before allowing them to cancel changes

    //Separate UI from core with assembly definition refs. BindingElement in particular references UI elements directly
    //This also means moving some code from PlayerInputRebinder into UI code (or the whole thing?)

    //Save changes

    //Store control schemes?
    public class PlayerInputRebinder : MonoBehaviour
    {
        private PlayerInputActions _inputActions;
        private PlayerInputActions _editActions;
        private PlayerInputActions _defaultActions;

        public event Action refresh;
        public event Action rebindComplete;
        public event Action rebindCancelled;
        public event Action<InputAction, int> rebindStarted;

        [SerializeField] private OkCancelMessageBox bindingInUseWarning;
        [SerializeField] private PauseMenuUIControls pauseMenuUIControls;
        [SerializeField] private List<GameObject> inputSourcesObjects;
        private List<IInputSource> _inputSources;
        private List<InputAction> overlappingBindings;

        private void Awake()
        {
            overlappingBindings = new List<InputAction>();
            _inputActions = new PlayerInputActions();
            _editActions = new PlayerInputActions();
            _defaultActions = new PlayerInputActions();
            InitInputSources();
        }

        public void ApplyChanges()
        {
            CopyActionBindings(_editActions, _inputActions);
        }

        public void CancelChanges()
        {
            CopyActionBindings(_inputActions, _editActions);
        }

        public void ResetToDefaults()
        {
            CopyActionBindings(_defaultActions, _editActions);
        }

        ///Input sources are initialized from here to prevent race conditions
        public void InitInputSources()
        {
            _inputSources = new List<IInputSource>();

            foreach (var sourceObj in inputSourcesObjects)
            {
                var inputSource = sourceObj.GetComponent<IInputSource>();
                _inputSources.Add(inputSource);
                inputSource.Initialize(_inputActions);
                (inputSource as MonoBehaviour).enabled = true;
            }
        }

        private void CopyActionBindings(PlayerInputActions copyFrom, PlayerInputActions copyTo)
        {
            var copyFromMaps = copyFrom.asset.actionMaps;
            var copyToMaps = copyTo.asset.actionMaps;

            for (int i = 0; i < copyFromMaps.Count; i++)
            {
                InputActionMap copyFromMap = copyFromMaps[i];
                InputActionMap copyToMap = copyToMaps[i];

                for (int j = 0; j < copyFromMap.actions.Count; j++)
                {
                    InputAction copyFromAction = copyFromMap.actions[j];
                    InputAction copyToAction = copyToMap.actions[j];

                    for (int k = 0; k < copyFromAction.bindings.Count; k++)
                    {
                        InputBinding copyFromBinding = copyFromAction.bindings[k];
                        copyToAction.ApplyBindingOverride(k, copyFromBinding);
                    }

                }
            }
            refresh?.Invoke();
        }

        #region DoRebind

        public void DoRebind(string actionName, int bindingIndex, Text statusText)
        {
            InputAction actionToRebind = _editActions.asset.FindAction(actionName);
            actionToRebind.Disable();
            var prevBinding = actionToRebind.bindings[0];
            var rebind = actionToRebind.PerformInteractiveRebinding(bindingIndex);

            rebind.OnComplete(operation =>
            {
                FinishRebinding(actionToRebind, operation, prevBinding);
            });

            rebind.OnCancel(operation =>
            {
                actionToRebind.Enable();
                operation.Dispose();

                rebindCancelled?.Invoke();
            });

            rebind.WithCancelingThrough("<Keyboard>/escape");

            rebindStarted?.Invoke(actionToRebind, bindingIndex);
            rebind.Start();
        }

        /// <summary> Returns the current binding text for the action and control scheme 
        /// corresponding to actionName and bindingIndex respectively </summary>
        public string GetBindingName(string actionName, int bindingIndex)
        {
            InputAction action = _editActions.asset.FindAction(actionName);
            return action.GetBindingDisplayString(bindingIndex);
        }

        private void FinishRebinding(InputAction actionToRebind,
            InputActionRebindingExtensions.RebindingOperation operation, InputBinding prevBinding)
        {
            operation.Cancel();
            actionToRebind.Enable();
            operation.Dispose();

            if (IsBindingInUse(actionToRebind))
            {
                ShowBindingInUseWarning(actionToRebind, prevBinding);
            }
            else
            {
                rebindComplete?.Invoke();
            }            
        }

        private bool IsBindingInUse(InputAction actionToRebind)
        {
            var newBinding = actionToRebind.bindings[0];

            var bindings = actionToRebind.actionMap.bindings;
            foreach (var binding in bindings)
            {
                if (binding.action == newBinding.action)
                    continue;
                if (binding.effectivePath == newBinding.effectivePath)
                    return true;
            }

            return false;
        }

        #endregion

        #region Binding in use warning

        private void ShowBindingInUseWarning(InputAction actionToRebind, InputBinding prevBinding)
        {
            string description = "The " + GetBindingName(prevBinding.action, 0) + " key is already bound to the "
                + prevBinding.action + " action. Would you like to continue?";
            bindingInUseWarning.gameObject.SetActive(true);
            pauseMenuUIControls.ShowRebindingLists(false);
            bindingInUseWarning.Initialize(description);

            bindingInUseWarning.onOKClicked += () => ConfirmRebinding(actionToRebind, prevBinding);
            bindingInUseWarning.onCancelClicked += () => CancelRebinding(actionToRebind, prevBinding);
        }

        private void ConfirmRebinding(InputAction actionToRebind, InputBinding prevBinding)
        {
            InputAction oldBoundAction = _editActions.asset.FindAction(prevBinding.action);
            if(!overlappingBindings.Contains(actionToRebind))
                overlappingBindings.Add(actionToRebind);
            if (!overlappingBindings.Contains(actionToRebind))
                overlappingBindings.Add(oldBoundAction);

            pauseMenuUIControls.ShowRebindingLists(true);            
            rebindComplete?.Invoke();
        }

        private void CancelRebinding(InputAction actionToRebind, InputBinding prevBinding)
        {
            pauseMenuUIControls.ShowRebindingLists(true);
            actionToRebind.ChangeBinding(0).To(prevBinding);
            rebindCancelled?.Invoke();

            actionToRebind.ChangeBinding(0);
        }


        #endregion
    }
}
