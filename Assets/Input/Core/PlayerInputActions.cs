// GENERATED AUTOMATICALLY FROM 'Assets/Input/Core/PlayerInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""ee84108e-944e-433c-a7cc-6b01529cd492"",
            ""actions"": [
                {
                    ""name"": ""MoveUP_KBM"",
                    ""type"": ""PassThrough"",
                    ""id"": ""032159b0-c005-42bf-bb65-6efa7d225e6b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveRight_KBM"",
                    ""type"": ""PassThrough"",
                    ""id"": ""cb32518a-0144-4b73-8eb6-0102910a8a01"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveLeft_KBM"",
                    ""type"": ""PassThrough"",
                    ""id"": ""69f45434-aa8a-40b3-a4dc-4381a25f5708"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveDown_KBM"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5c2847ee-e2a0-4f22-95e4-c33be3a9eb93"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move_Gamepad"",
                    ""type"": ""PassThrough"",
                    ""id"": ""4764e8ca-a252-401d-be60-2f51fcf0434b"",
                    ""expectedControlType"": ""Stick"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""PassThrough"",
                    ""id"": ""6a1ca064-6e3a-4e37-b2f3-04f84d72e4d2"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FirePrimary"",
                    ""type"": ""Button"",
                    ""id"": ""9d834501-4dbe-4eda-b03b-75cf98437dd0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FireSecondary"",
                    ""type"": ""Button"",
                    ""id"": ""17df62ec-7d14-40f3-aff0-ca4c38b6bfbb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ContextualMoveAbility"",
                    ""type"": ""PassThrough"",
                    ""id"": ""64162274-656c-4de9-aad3-9eee8c592198"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""68104f5d-366f-4494-a6fe-c0ed4c48da21"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""31363930-e75f-4e99-a6c2-52b60fe25f77"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""StickDeadzone(min=0.01,max=1)"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c3b9f250-20a6-4286-9dfd-f9eacad6bca8"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""FirePrimary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4273a86f-74f9-4c5e-9889-178ac205fec2"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""FirePrimary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f035998b-dc85-441d-95c9-624d615f4412"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""FireSecondary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c40996ed-c033-4d3c-81e7-aa622642d4c2"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""FireSecondary"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""927f2be6-2719-4aba-88de-ce1388fa9847"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move_Gamepad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""acfcfa8a-c912-42a0-b952-026734b89244"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""MoveUP_KBM"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f2383bcc-aa0c-4fb8-8d8f-15821389b3e7"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""MoveRight_KBM"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""357cc0e4-f21c-487b-8ab8-3d505f91ec3d"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""MoveLeft_KBM"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f17741ee-a8b2-4bce-89bc-91fa26027fba"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""MoveDown_KBM"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d0d4fe35-5024-4054-b8c9-179e03124ab8"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""ContextualMoveAbility"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d8cc6b63-c203-4086-b44d-59f13f55e621"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ContextualMoveAbility"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menus"",
            ""id"": ""5f1bf9ad-1581-4a8e-b033-fd2d9c4f2810"",
            ""actions"": [
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""2cc36a2e-078d-4258-be99-09f1b3409293"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""92438832-96d2-4b69-8f14-54ebfce7c897"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyboardAndMouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8cea22ec-fb54-480b-89dc-c684c3f4c4e4"",
                    ""path"": ""<DualShockGamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8756403f-3d06-4a65-a6b5-b10dc6c9fac1"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyboardAndMouse"",
            ""bindingGroup"": ""KeyboardAndMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_MoveUP_KBM = m_Player.FindAction("MoveUP_KBM", throwIfNotFound: true);
        m_Player_MoveRight_KBM = m_Player.FindAction("MoveRight_KBM", throwIfNotFound: true);
        m_Player_MoveLeft_KBM = m_Player.FindAction("MoveLeft_KBM", throwIfNotFound: true);
        m_Player_MoveDown_KBM = m_Player.FindAction("MoveDown_KBM", throwIfNotFound: true);
        m_Player_Move_Gamepad = m_Player.FindAction("Move_Gamepad", throwIfNotFound: true);
        m_Player_Look = m_Player.FindAction("Look", throwIfNotFound: true);
        m_Player_FirePrimary = m_Player.FindAction("FirePrimary", throwIfNotFound: true);
        m_Player_FireSecondary = m_Player.FindAction("FireSecondary", throwIfNotFound: true);
        m_Player_ContextualMoveAbility = m_Player.FindAction("ContextualMoveAbility", throwIfNotFound: true);
        // Menus
        m_Menus = asset.FindActionMap("Menus", throwIfNotFound: true);
        m_Menus_Pause = m_Menus.FindAction("Pause", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_MoveUP_KBM;
    private readonly InputAction m_Player_MoveRight_KBM;
    private readonly InputAction m_Player_MoveLeft_KBM;
    private readonly InputAction m_Player_MoveDown_KBM;
    private readonly InputAction m_Player_Move_Gamepad;
    private readonly InputAction m_Player_Look;
    private readonly InputAction m_Player_FirePrimary;
    private readonly InputAction m_Player_FireSecondary;
    private readonly InputAction m_Player_ContextualMoveAbility;
    public struct PlayerActions
    {
        private @PlayerInputActions m_Wrapper;
        public PlayerActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @MoveUP_KBM => m_Wrapper.m_Player_MoveUP_KBM;
        public InputAction @MoveRight_KBM => m_Wrapper.m_Player_MoveRight_KBM;
        public InputAction @MoveLeft_KBM => m_Wrapper.m_Player_MoveLeft_KBM;
        public InputAction @MoveDown_KBM => m_Wrapper.m_Player_MoveDown_KBM;
        public InputAction @Move_Gamepad => m_Wrapper.m_Player_Move_Gamepad;
        public InputAction @Look => m_Wrapper.m_Player_Look;
        public InputAction @FirePrimary => m_Wrapper.m_Player_FirePrimary;
        public InputAction @FireSecondary => m_Wrapper.m_Player_FireSecondary;
        public InputAction @ContextualMoveAbility => m_Wrapper.m_Player_ContextualMoveAbility;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @MoveUP_KBM.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveUP_KBM;
                @MoveUP_KBM.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveUP_KBM;
                @MoveUP_KBM.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveUP_KBM;
                @MoveRight_KBM.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveRight_KBM;
                @MoveRight_KBM.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveRight_KBM;
                @MoveRight_KBM.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveRight_KBM;
                @MoveLeft_KBM.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveLeft_KBM;
                @MoveLeft_KBM.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveLeft_KBM;
                @MoveLeft_KBM.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveLeft_KBM;
                @MoveDown_KBM.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveDown_KBM;
                @MoveDown_KBM.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveDown_KBM;
                @MoveDown_KBM.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveDown_KBM;
                @Move_Gamepad.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove_Gamepad;
                @Move_Gamepad.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove_Gamepad;
                @Move_Gamepad.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove_Gamepad;
                @Look.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                @FirePrimary.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFirePrimary;
                @FirePrimary.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFirePrimary;
                @FirePrimary.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFirePrimary;
                @FireSecondary.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFireSecondary;
                @FireSecondary.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFireSecondary;
                @FireSecondary.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFireSecondary;
                @ContextualMoveAbility.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnContextualMoveAbility;
                @ContextualMoveAbility.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnContextualMoveAbility;
                @ContextualMoveAbility.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnContextualMoveAbility;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @MoveUP_KBM.started += instance.OnMoveUP_KBM;
                @MoveUP_KBM.performed += instance.OnMoveUP_KBM;
                @MoveUP_KBM.canceled += instance.OnMoveUP_KBM;
                @MoveRight_KBM.started += instance.OnMoveRight_KBM;
                @MoveRight_KBM.performed += instance.OnMoveRight_KBM;
                @MoveRight_KBM.canceled += instance.OnMoveRight_KBM;
                @MoveLeft_KBM.started += instance.OnMoveLeft_KBM;
                @MoveLeft_KBM.performed += instance.OnMoveLeft_KBM;
                @MoveLeft_KBM.canceled += instance.OnMoveLeft_KBM;
                @MoveDown_KBM.started += instance.OnMoveDown_KBM;
                @MoveDown_KBM.performed += instance.OnMoveDown_KBM;
                @MoveDown_KBM.canceled += instance.OnMoveDown_KBM;
                @Move_Gamepad.started += instance.OnMove_Gamepad;
                @Move_Gamepad.performed += instance.OnMove_Gamepad;
                @Move_Gamepad.canceled += instance.OnMove_Gamepad;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @FirePrimary.started += instance.OnFirePrimary;
                @FirePrimary.performed += instance.OnFirePrimary;
                @FirePrimary.canceled += instance.OnFirePrimary;
                @FireSecondary.started += instance.OnFireSecondary;
                @FireSecondary.performed += instance.OnFireSecondary;
                @FireSecondary.canceled += instance.OnFireSecondary;
                @ContextualMoveAbility.started += instance.OnContextualMoveAbility;
                @ContextualMoveAbility.performed += instance.OnContextualMoveAbility;
                @ContextualMoveAbility.canceled += instance.OnContextualMoveAbility;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);

    // Menus
    private readonly InputActionMap m_Menus;
    private IMenusActions m_MenusActionsCallbackInterface;
    private readonly InputAction m_Menus_Pause;
    public struct MenusActions
    {
        private @PlayerInputActions m_Wrapper;
        public MenusActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Pause => m_Wrapper.m_Menus_Pause;
        public InputActionMap Get() { return m_Wrapper.m_Menus; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenusActions set) { return set.Get(); }
        public void SetCallbacks(IMenusActions instance)
        {
            if (m_Wrapper.m_MenusActionsCallbackInterface != null)
            {
                @Pause.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnPause;
            }
            m_Wrapper.m_MenusActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
            }
        }
    }
    public MenusActions @Menus => new MenusActions(this);
    private int m_KeyboardAndMouseSchemeIndex = -1;
    public InputControlScheme KeyboardAndMouseScheme
    {
        get
        {
            if (m_KeyboardAndMouseSchemeIndex == -1) m_KeyboardAndMouseSchemeIndex = asset.FindControlSchemeIndex("KeyboardAndMouse");
            return asset.controlSchemes[m_KeyboardAndMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnMoveUP_KBM(InputAction.CallbackContext context);
        void OnMoveRight_KBM(InputAction.CallbackContext context);
        void OnMoveLeft_KBM(InputAction.CallbackContext context);
        void OnMoveDown_KBM(InputAction.CallbackContext context);
        void OnMove_Gamepad(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnFirePrimary(InputAction.CallbackContext context);
        void OnFireSecondary(InputAction.CallbackContext context);
        void OnContextualMoveAbility(InputAction.CallbackContext context);
    }
    public interface IMenusActions
    {
        void OnPause(InputAction.CallbackContext context);
    }
}
