using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerInput
{
    public class UiInputSource : MonoBehaviour, PlayerInput.Rebinding.IInputSource
    {
        [SerializeField] private PauseMenuUIControls _pauseMenuControls;
        private InputAction _pauseMenuAct;

        ///Actions
        private PlayerInputActions _inputActions;

        #region Activate / deactivate

        private void OnEnable()
        {
            EnableActions();
        }

        private void OnDisable()
        {
            DisableActions();
        }

        #endregion

        private void PauseGame(InputAction.CallbackContext context)
        {
            _pauseMenuControls.DoEscape();
        }

        public void Initialize(PlayerInputActions inputActions)
        {
            _inputActions = inputActions;
        }

        public void EnableActions()
        {
            _pauseMenuAct = _inputActions.Menus.Pause;

            _pauseMenuAct.canceled += PauseGame;
            _pauseMenuAct.Enable();
        }

        private void DisableActions()
        {
            _pauseMenuAct.Disable();
        }
    }
}