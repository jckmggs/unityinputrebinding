using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerInput
{
    public interface ILookAndMove
    {
        public void updateContinuousInput(Vector2 moveInputRaw, float moveInputMagnitude, Vector2 mouseDeltaRaw);
    }

    public interface IContextualMoveAbility
    {
        public void triggerContextualMoveAbility();
    }

    public class CharacterControllerInputSource : MonoBehaviour, Rebinding.IInputSource
    {
        private bool _inputEnabled = false;
        private ILookAndMove _controller = null;

        [SerializeField] private Rebinding.PlayerInputRebinder _inputRebinder;
        [SerializeField] private PlayerInputActions _inputActions;

        ///Movement:
        private InputAction _moveUpAct;
        private InputAction _moveRightAct;
        private InputAction _moveLeftAct;
        private InputAction _moveDownAct;
        private InputAction _moveGamepad;
        private float _moveUp;
        private float _moveRight;
        private float _moveDown;
        private float _moveLeft;

        private float _moveInputMagnitude = 1f;
        private Vector2 _moveInputDirection;

        private InputAction _look;
        private Vector2 _mouseDeltaRaw;

        private InputAction _contextualMoveAbility;

        public void Initialize(PlayerInputActions inputActions)
        {
            _inputActions = inputActions;
        }

        #region Activate / deactivate

        private void Awake()
        {
            _controller = gameObject.GetComponent<ILookAndMove>();
#if UNITY_EDITOR
            if (_controller == null)
            {
                Debug.LogWarning("Error! CharacterControllerInputSource source has no controller");
            }
#endif
        }

        private void OnEnable()
        {
            EnableActions();
        }

        private void OnDisable()
        {
            DisableActions();
        }

        public void EnableActions()
        {
            if (_inputEnabled)
                return;
            _inputEnabled = true;

            _moveUpAct = _inputActions.Player.MoveUP_KBM;
            _moveUpAct.performed += OnMoveUp;
            _moveUpAct.Enable();

            _moveRightAct = _inputActions.Player.MoveRight_KBM;
            _moveRightAct.performed += OnMoveRight;
            _moveRightAct.Enable();

            _moveLeftAct = _inputActions.Player.MoveLeft_KBM;
            _moveLeftAct.performed += OnMoveLeft;
            _moveLeftAct.Enable();

            _moveDownAct = _inputActions.Player.MoveDown_KBM;
            _moveDownAct.performed += OnMoveDown;
            _moveDownAct.Enable();

            _moveGamepad = _inputActions.Player.Move_Gamepad;
            _moveGamepad.performed += OnMoveGamepad;
            _moveGamepad.Enable();

            _look = _inputActions.Player.Look;
            _look.performed += OnLook;
            _look.Enable();

            _contextualMoveAbility = _inputActions.Player.ContextualMoveAbility;
            _contextualMoveAbility.performed += OnContextualMoveAbility;
            _contextualMoveAbility.Enable();
        }

        private void DisableActions()
        {
            if (_inputEnabled)
            {
                _moveUpAct.Disable();
                _moveRightAct.Disable();
                _moveLeftAct.Disable();
                _moveDownAct.Disable();
                _moveGamepad.Disable();
                _look.Disable();
                _contextualMoveAbility.Disable();

                _inputEnabled = false;
            }
        }

        #endregion

        #region Update
        private void Update()
        {
            if (_inputEnabled)
            {
                //_controller.updateContinuousInput(_moveInputRaw, _moveInputMagnitude, _mouseDeltaRaw);
            }
        }

        #endregion

        #region Input Callbacks

        public void OnMoveUp(InputAction.CallbackContext context)
        {
            _moveUp = context.ReadValue<float>();
            _moveInputMagnitude = 1;
        }

        public void OnMoveRight(InputAction.CallbackContext context)
        {
            _moveUp = context.ReadValue<float>();
            _moveInputMagnitude = 1;
        }

        public void OnMoveLeft(InputAction.CallbackContext context)
        {
            _moveUp = context.ReadValue<float>();
            _moveInputMagnitude = 1;
        }

        public void OnMoveDown(InputAction.CallbackContext context)
        {
            _moveUp = context.ReadValue<float>();
            _moveInputMagnitude = 1;
        }

        public void OnMoveGamepad(InputAction.CallbackContext context)
        {
            _moveInputDirection = context.ReadValue<Vector2>();
            _moveInputMagnitude = _moveInputDirection.magnitude;
            _moveInputDirection.Normalize();
        }

        public void OnLook(InputAction.CallbackContext context)
        {
            _mouseDeltaRaw = 10f * context.ReadValue<Vector2>();
        }

        public void OnContextualMoveAbility(InputAction.CallbackContext context)
        {
            int c = 1;
        }

        #endregion
    }
}