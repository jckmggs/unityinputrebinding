using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuUIControls : MonoBehaviour
{
    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private GameObject _rebindingMenu;
    [SerializeField] private GameObject _bindingLists;
    [SerializeField] private GameObject _keyboardRebindMenu;
    [SerializeField] private GameObject _gamepadRebindMenu;
    [SerializeField] private PlayerInput.Rebinding.PlayerInputRebinder _rebinder;

    private EscapeKeyState _escapeKeyState;
    private PauseMenuEscapeState _pauseMenuEscape;
    private RebindingMenuEscapeState _rebindingMenuEscape;
    private UnpausedEscapeState _unpausedEscape;

    private void Awake()
    {
        _pauseMenuEscape = new PauseMenuEscapeState(this);
        _rebindingMenuEscape = new RebindingMenuEscapeState(this);
        _unpausedEscape = new UnpausedEscapeState(this);

        _escapeKeyState = _unpausedEscape;
    }

    public void DoEscape()
    {
        _escapeKeyState.DoEscape();
    }

    #region Pause Menu

    public void ShowPauseMenu()
    {
        _escapeKeyState = _pauseMenuEscape;

        _pauseMenu.SetActive(true);
    }

    public void ClosePauseMenu()
    {
        _escapeKeyState = _unpausedEscape;

        _pauseMenu.SetActive(false);
        _rebindingMenu.SetActive(false);
        _bindingLists.SetActive(false);
        _keyboardRebindMenu.SetActive(false);
        _gamepadRebindMenu.SetActive(false);
    }

    #endregion

    #region Rebinding Menu

    public void ShowRebindingMenu()
    {
        _escapeKeyState = _rebindingMenuEscape;

        _pauseMenu.SetActive(false);
        _rebindingMenu.SetActive(true);
        _bindingLists.SetActive(true);
        ShowKBMRebindingMenu();
    }

    public void ApplyRebindingMenuChanges()
    {
        _rebinder.ApplyChanges();
        CloseRebindingMenu();
        ShowPauseMenu();
    }

    public void CancelRebindingMenuChanges()
    {
        _rebinder.CancelChanges();
        CloseRebindingMenu();
        ShowPauseMenu();
    }

    public void CloseRebindingMenu()
    {
        _rebindingMenu.SetActive(false);
        _bindingLists.SetActive(false);
        _keyboardRebindMenu.SetActive(false);
        _gamepadRebindMenu.SetActive(false);
    }

    public void ShowRebindingLists(bool show)
    {
        _bindingLists.SetActive(show);
    }

    public void ShowKBMRebindingMenu()
    {
        _escapeKeyState = _rebindingMenuEscape;

        _keyboardRebindMenu.SetActive(true);
        _gamepadRebindMenu.SetActive(false);
    }

    public void ShowGamepadRebindingMenu()
    {
        _escapeKeyState = _rebindingMenuEscape;

        _keyboardRebindMenu.SetActive(false);
        _gamepadRebindMenu.SetActive(true);
    }
    #endregion
}

#region Escape key state machine

public abstract class EscapeKeyState
{
    protected PauseMenuUIControls _pauseMenuUIControls;

    public EscapeKeyState(PauseMenuUIControls pauseMenuUIControls)
    {
        _pauseMenuUIControls = pauseMenuUIControls;
    }

    public abstract void DoEscape();
}

public class UnpausedEscapeState : EscapeKeyState
{
    public UnpausedEscapeState(PauseMenuUIControls pauseMenuUIControls) 
        : base(pauseMenuUIControls)
    {
        
    }

    public override void DoEscape()
    {
        _pauseMenuUIControls.ShowPauseMenu();
    }
}

public class PauseMenuEscapeState : EscapeKeyState
{
    public PauseMenuEscapeState(PauseMenuUIControls pauseMenuUIControls) 
        : base(pauseMenuUIControls)
    {
        
    }

    public override void DoEscape()
    {
        _pauseMenuUIControls.ClosePauseMenu();
    }
}

public class RebindingMenuEscapeState : EscapeKeyState
{
    public RebindingMenuEscapeState(PauseMenuUIControls pauseMenuUIControls) 
        : base(pauseMenuUIControls)
    {
        
    }

    public override void DoEscape()
    {
        _pauseMenuUIControls.CancelRebindingMenuChanges();
    }
}

#endregion